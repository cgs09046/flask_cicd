from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/', methods=['GET'])
def print_name():
    return "hello bob from gunwoo Kim"

@app.route('/add', methods=['GET'])
def add():   
    a = request.args.get('a')
    b = request.args.get('b')
    result = str(int(a) + int(b))
    return result


@app.route('/sub', methods=['GET'])
def sub():
    a = request.args.get('a')
    b = request.args.get('b')
    result =  str(int(a) - int(b))
    return result

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8012)