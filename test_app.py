import requests
import app, unittest, random


class Flasktest(unittest.TestCase):
    def setUp(self):
        self.host = "http://localhost:8012"
        
        
    def test_add(self):
        a = random.randint(0, 1000)
        b = random.randint(0, 1000)
        self.param = {
            'a' : a,
            'b' : b
        }
        result = requests.get(self.host+ "/add", params = self.param)
        data = result.text
        self.assertEqual(a+b, int(data))

    def test_sub(self):
        a = random.randint(0, 1000)
        b = random.randint(0, 1000)
        self.param = {
            'a' : a,
            'b' : b
        }
        result = requests.get(self.host+ "/sub", params = self.param)
        data = result.text
        self.assertEqual(a-b, int(data))

if __name__ == '__main__':
    unittest.main()